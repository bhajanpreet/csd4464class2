package com.cestar.main;

import java.sql.SQLException;

import com.cestar.dao.GenericDAO;
import com.cestar.model.Item;

/**
 * Class to test run the application we build in class 2 of CSD4464
 * @author Bhajanpreet Singh
 *  
 */

public class Runner {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        GenericDAO dao = new GenericDAO("Item", Item.mapper);
        
        for(Object i : dao.getInformation()) {
           Item it = (Item)i;
           System.out.println(it.toString());
        }
        
    }
}
