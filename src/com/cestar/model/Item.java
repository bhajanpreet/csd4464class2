package com.cestar.model;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.cestar.dao.Mapper;

/**
 * Model class for interactivity for the Item entity in the database
 * @author bhajanpreet
 *
 */
public class Item extends Model{
    
    public Item(int id, String name, double price, String company) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
        this.company = company;
    }
    
    public static Mapper mapper = (ResultSet rs, String tableName) -> {
        ArrayList<Object> items = new ArrayList<Object>();
        while(rs.next()) {
            items.add(new Item(rs.getInt(1),rs.getString(2),rs.getDouble(3),rs.getString(4)));
        }
        return items;
    };
    
    private int id;
    private String name;
    private double price;
    private String company;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }
    
    @Override
    public String toString() {
        return "Item [id=" + id + ", name=" + name + ", price=" + price + ", company=" + company + "]";
    }
    
    
}
