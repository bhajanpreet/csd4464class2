package com.cestar.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@FunctionalInterface
public interface Mapper {
    ArrayList<Object> map (ResultSet rs, String tableName) throws SQLException;
}
