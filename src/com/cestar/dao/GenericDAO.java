package com.cestar.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GenericDAO{
    
    public String className;
    
    public Mapper mapper;
    
    public GenericDAO(String tableName, Mapper mapper) throws ClassNotFoundException {
        className = tableName;
        this.mapper = mapper;
        
    }
   
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://127.0.0.1:3306/cabin";
        Connection con = DriverManager.getConnection(url, "root", "rootpw");
        // System.out.println("Connected");
        return con;
    }   

    public ArrayList<Object> getInformation() throws ClassNotFoundException, SQLException{
        Connection conn = getConnection();
        String sql = "SELECT * FROM "+ className;
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        return mapper.map(rs, className);
    }

}
